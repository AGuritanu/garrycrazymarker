	var raster = new ol.layer.Tile({
        source: new ol.source.OSM()
      });
    var source = new ol.source.Vector({wrapX: false});
	var vector = new ol.layer.Vector({
        source: source
      });
	var coordinatesPuhoi=ol.proj.transform([29.028131,46.820684], 'EPSG:4326', 'EPSG:3857');
    var map = new ol.Map({
        layers: [raster, vector],
        target: 'map',
        view: new ol.View({
           center:coordinatesPuhoi ,
           zoom:14  
        })
      }); 
    var draw = new ol.interaction.Draw({
            source: source,
            type: /** @type {ol.geom.GeometryType} */ ("Polygon")
           });
    map.addInteraction(draw);